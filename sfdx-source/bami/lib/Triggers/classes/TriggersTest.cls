@IsTest
public with sharing class TriggersTest {

    public static Boolean isBeforeInsertForced {
        get {
            if (isBeforeInsertForced == null)
                isBeforeInsertForced = false;
            return isBeforeInsertForced;
        }
        set;
    }

    public static Boolean isAfterInsertForced {
        get {
            if (isAfterInsertForced == null)
                isAfterInsertForced = false;
            return isAfterInsertForced;
        }
        set;
    }

    public static Boolean isBeforeUpdateForced {
        get {
            if (isBeforeUpdateForced == null)
                isBeforeUpdateForced = false;
            return isBeforeUpdateForced;
        }
        set;
    }

    public static Boolean isAfterUpdateForced {
        get {
            if (isAfterUpdateForced == null)
                isAfterUpdateForced = false;
            return isAfterUpdateForced;
        }
        set;
    }

    public static Boolean isBeforeDeleteForced {
        get {
            if (isBeforeDeleteForced == null)
                isBeforeDeleteForced = false;
            return isBeforeDeleteForced;
        }
        set;
    }

    public static Boolean isAfterDeleteForced {
        get {
            if (isAfterDeleteForced == null)
                isAfterDeleteForced = false;
            return isAfterDeleteForced;
        }
        set;
    }

    public static Boolean isUndeleteForced {
        get {
            if (isUndeleteForced == null)
                isUndeleteForced = false;
            return isUndeleteForced;
        }
        set;
    }


    @IsTest
    public static void manageTest() {
        Triggers triggers = new Triggers();

        System.Test.startTest();

        isBeforeInsertForced = true;
        triggers.manage();
        isBeforeInsertForced = false;

        isAfterInsertForced = true;
        triggers.manage();
        isAfterInsertForced = false;

        isBeforeUpdateForced = true;
        triggers.manage();
        isBeforeUpdateForced = false;

        isAfterUpdateForced = true;
        triggers.manage();
        isAfterUpdateForced = false;

        isBeforeDeleteForced = true;
        triggers.manage();
        isBeforeDeleteForced = false;

        isAfterDeleteForced = true;
        triggers.manage();
        isAfterDeleteForced = false;

        isUndeleteForced = true;
        triggers.manage();
        isUndeleteForced = false;

        System.Test.stopTest();


    }
}